<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8">
				<ul>
					<li><h4>Bánh cuốn D2</h4></li>
					<li><i class="fa fa-map-marker"></i> Hà Nội</li>
					<li><i class="fa fa-phone"></i> 0123 456 789</li>
					<li><i class="fa fa-envelope"></i> example@gmail.com</li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-2">
				<ul class="list-item">
					<li></li>
					<li><a href="#">Giới Thiệu</a></li>
					<li><a href="#">Hệ Thống Cửa Hàng</a></li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-2">
				<ul class="list-item">
					<li></li>
					<li><a href="#" class="fa fa-facebook"> Facebook</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="bottom">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-9">Copyright © 2017 H2T . All Rights Reserved.</div>
			</div>
		</div>
	</div>
</footer>