<div id="header">
	<div class="container">
		<div class="col-xs-12 col-sm-4 col-md-3 text-left">
			<div class="logo">
				<a href="#" title="Banh cuon D2">
					<img class="img-responsive" src="uploads/logo-banhcuongiaan.png" alt="logo-banhcuongiaan">
				</a>
			</div>
		</div>
		<div class="col-sx-12 col-sm-8 col-md-9 text-right">
			<div class="header-contact">
				<div class="icon-phone">
					<i class="fa fa-phone"></i>
				</div>
				<div class="info">
					<span>Giao hàng tận nơi</span>
					<p><strong>0123 456 789</strong></p>
				</div>
			</div><!-- end header-contact -->
			<!-- Form search -->
			<form class="navbar-form navbar-right form-search">
				 <div class="input-group">
				    <input type="text" class="form-control" placeholder="Tìm kiếm sản phẩm">
				    <div class="input-group-btn">
				      <button class="btn btn-default" type="submit">
				        <i class="glyphicon glyphicon-search"></i>
				      </button>
				    </div>
				</div>
			</form>
			<!-- End form search -->
		</div>
	</div>
</div><!-- End header -->
<div class="menu">
	<nav class="navbar navbar-default menu-nav">
		<div class="container">
		    <ul class="nav navbar-nav menu-navbar">
			    <li><a href="#">Trang chủ</a></li>
			    <li class="dropdown">
			        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Giới Thiệu
			        <ul class="dropdown-menu">
			          <li><a href="#">Giới Thiệu Chung</a></li>
			        </ul>
			    </li>
			    <li><a href="#">Sản Phẩm</a></li>

			    <li><a href="/bang-gia.show">Bảng Giá</a></li>
			    <li><a href="/shopsystem.show">Hệ Thống Cửa Hàng</a></li>
		    </ul>
		</div>
	</nav>
</div>