@extends('layouts.app')
@section('content')
<div class="container">
<div class="col-md-9">
	<div class="breadCrumb">
		<span>
			<i class="fa fa-home" aria-hidden="true"> </i>
			<span itemprop="title">Trang chủ </span> ›

		</span>
		<span>
		 Bảng giá bánh cuốn Phương Anh 
		</span>
	</div>
	<h1 class="post-title">
	 Bảng giá bánh cuốn Phương Anh 
	</h1>
	<!-- Hien thi table do an  -->
	<table>
		  <tr>
		    <th>Đồ ăn</th>
		    <th>Giá</th>
		  </tr>
		  <tr>
		    <td>Bánh cuốn chay</td>
		    <td>17000đ/suất</td>
		  </tr>
		  <tr>
		    <td>Bánh cuốn chay-thịt nướng/thịt viên/chả quế</td>
		    <td>25000đ/suất</td>
		  </tr>
		  <tr>
		    <td>Bánh cuốn chay-chả mực</td>
		    <td>35000đ/suất</td>
		  </tr>
		  <tr>
		    <td>Bánh cuốn nhân thịt</td>
		    <td>20000đ/suất</td>
		  </tr>
		  <tr>
		    <td>Bánh cuốn nhân thịt-chả nướng/thịt viên/chả quế	</td>
		    <td>27000đ/suất</td>
		  </tr>
		  <tr>
		    <td>Bánh cuốn nhân thịt- chả mực</td>
		    <td>40000đ/suất</td>
		  </tr>
		  <tr>
		    <td>Bánh cuốn chay-chả nướng/thịt viên/chả quế-ruốc	</td>
		    <td>27000đ/suất</td>
		  </tr>
		  <tr>
		    <td>Bánh cuốn chay-chả mực-ruốc	</td>
		    <td>37000đ/suất</td>
		  </tr>
		  <tr>
		    <td>Bánh cuốn nhân thịt-chả nướng/thịt viên/chả quế-ruốc</td>
		    <td>29000đ/suất</td>
		  </tr>
		  <tr>
		    <td>Bánh cuốn nhân thịt-chả mực-ruốc</td>
		    <td>43000đ/suất</td>
		  </tr>
	</table>

	<h1 class="post-title">
	 Đồ uống & các loại đồ ăn kèm
	</h1>
	<!-- Hien thi table do an  -->
	<table>
		  <tr>
		    <th>Đồ uống</th>
		    <th>Giá</th>
		  </tr>
		  <tr>
		    <td>Bia Hà Nội</td>
		    <td>15000đ/1 chai</td>
		  </tr>
		  <tr>
		    <td>Bia Sài Gòn</td>
		    <td>20000đ/1 chai</td>
		  </tr>
		  <tr>
		    <td>Coca Cola</td>
		    <td>10000đ/1 chai</td>
		  </tr>
		  <tr>
		    <td>Pepsi</td>
		    <td>10000đ/1 chai</td>
		  </tr>
		  <tr>
		    <td>Trà Đá/Nước Vối	</td>
		    <td>3000đ/1 cốc</td>
		  </tr>
		  <tr>
		    <td>Sữa Đậu Lành</td>
		    <td>7000đ/1 cốc</td>
		  </tr>
		  <tr>
		    <td>Trứng Hấp</td>
		    <td>7000đ/1 quả</td>
		  </tr>
		  <tr>
		    <td>Lạc Sườn Rán</td>
		    <td>10000đ/1 cái</td>
		  </tr>
	</table>
</div>
<div class="col-md-3" id="col_complementary">
	@include('col_acticle_video')
</div>
	
</div>
@endsection