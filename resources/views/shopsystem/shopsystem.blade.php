@extends('layouts.app')
@section('content')
<div class="container">
<div class="col-md-9">
	<div class="breadCrumb">
		<span>
			<i class="fa fa-home" aria-hidden="true"> </i>
			<span itemprop="title">Trang chủ </span> ›

		</span>
		<span>
		Hệ thống cửa hàng
		</span>
	</div>
	<h1 class="post-title">
	 Hệ thống cửa hàng
	</h1>
	<div class="address">
		<p>Bánh cuốn Phương Anh </p>
		<p><i class="fa fa-home pr-10"> </i> Địa chỉ: 101 B1 Tập Thể Nghĩa Tân, Khu tập thể Nghĩa Tân, Nghĩa Tân, Cầu Giấy, Hà Nội</p>
		<p><i class="fa fa-phone pr-10"> </i> Điện thoại: +84 93 481 36 89</p>
		<p><i class="fa fa-envelope pr-10"> </i> Email: banhcuonphuonganh@gmail.com</p>
	</div>
	
</div>

<div class="col-md-3" id="col_complementary">
	@include('col_acticle_video')
</div>
	
</div>
@endsection